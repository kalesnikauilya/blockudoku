import os
import pathlib
import time
from os.path import isfile, join

import cv2
from PIL import ImageDraw, Image
from PIL import ImageFont
from matplotlib import colors as m_colors

from engine import pieces
from engine.game_parameters import BOARD_SIZE

HW = 700
SQUARE_SIZE = 40
BOARD_TOP_LEFT = (20, 20)
PIECES_TOP_LEFT = (20, 440)
STATISTICS_TOP_LEFT = (400, 20)
PIECES_AREA = 220


BACKGROUND_COLOR = m_colors.CSS4_COLORS['moccasin']
PIECE_COLOR = m_colors.CSS4_COLORS['saddlebrown']
EMPTY_COLOR = m_colors.CSS4_COLORS['sandybrown']
BORDER_COLOR = m_colors.CSS4_COLORS['cornflowerblue']
SQUARE_BOARDER_COLOR = m_colors.CSS4_COLORS['mediumblue']
TEXT_COLOR = m_colors.CSS4_COLORS['indigo']


class Visualizer:
    def __init__(self, seed):
        self.images = []
        self.seed = seed
        self.timestamp = time.strftime("%Y%m%d-%H%M%S")
        self.save_dir = "{}{}{}{}".format('out/', time.strftime("%Y%m%d-%H%M%S-"), seed, '/')
        self.save_images_dir = "{}{}".format(self.save_dir, 'images/')
        self.create_dirs()

    def create_dirs(self):
        p = pathlib.Path(self.save_images_dir)
        p.mkdir(parents=True, exist_ok=True)

    def create_video(self):
        frame_size = (HW, HW)
        fps = 1
        video_path = "{}{}".format(self.save_dir, 'output_video.mp4')
        out = cv2.VideoWriter(video_path, cv2.VideoWriter_fourcc(*'MP4V'), fps, frame_size)
        files = [f for f in os.listdir(self.save_images_dir) if isfile(join(self.save_images_dir, f))]
        files.sort()
        for f in files:
            filename = "{}{}".format(self.save_images_dir, f)
            img = cv2.imread(filename)
            out.write(img)
        out.release()

    def create_image(self, step, board):
        img = Image.new('RGBA', (HW, HW))
        draw = ImageDraw.Draw(img)

        # background
        draw.rectangle((0, 0, HW, HW), fill=BACKGROUND_COLOR)

        # board
        for row in range(BOARD_SIZE):
            for col in range(BOARD_SIZE):
                square_color = EMPTY_COLOR if board.is_free((row, col)) else PIECE_COLOR
                draw.rectangle((BOARD_TOP_LEFT[0] + row * SQUARE_SIZE, BOARD_TOP_LEFT[1] + col * SQUARE_SIZE,
                                BOARD_TOP_LEFT[0] + (row + 1) * SQUARE_SIZE,
                                BOARD_TOP_LEFT[1] + (col + 1) * SQUARE_SIZE),
                               fill=square_color, outline=BORDER_COLOR)

        # 3x3 square borders
        for row in [0, 3, 6, 9]:
            draw.line((BOARD_TOP_LEFT[0], BOARD_TOP_LEFT[1] + row * SQUARE_SIZE,
                       BOARD_TOP_LEFT[0] + BOARD_SIZE * SQUARE_SIZE, BOARD_TOP_LEFT[1] + row * SQUARE_SIZE),
                      fill=SQUARE_BOARDER_COLOR, width=2)
        for col in [0, 3, 6, 9]:
            draw.line((BOARD_TOP_LEFT[0] + col * SQUARE_SIZE, BOARD_TOP_LEFT[1],
                       BOARD_TOP_LEFT[0] + col * SQUARE_SIZE, BOARD_TOP_LEFT[1] + BOARD_SIZE * SQUARE_SIZE),
                      fill=SQUARE_BOARDER_COLOR, width=2)

        # pieces
        for piece_number, piece_id in enumerate(board.ordered_pieces):
            piece = pieces.pieces_rect[piece_id]
            for row in range(pieces.PIECE_SIZE):
                for col in range(pieces.PIECE_SIZE):
                    square_color = EMPTY_COLOR if piece[row][col] == 0 or \
                                                  piece_id not in board.available_pieces else PIECE_COLOR
                    draw.rectangle((PIECES_TOP_LEFT[0] + piece_number * PIECES_AREA + row * SQUARE_SIZE,
                                    PIECES_TOP_LEFT[1] + col * SQUARE_SIZE,
                                    PIECES_TOP_LEFT[0] + piece_number * PIECES_AREA + (row + 1) * SQUARE_SIZE,
                                    PIECES_TOP_LEFT[1] + (col + 1) * SQUARE_SIZE),
                                   fill=square_color, outline=BORDER_COLOR)

        # statistics
        stats = ''
        stats += "{}{}\n".format("Score = ", board.score)
        stats += "{}{}\n".format("Step = ", step)
        stats += "{}{}\n".format("Streak = ", board.streak)

        draw.multiline_text((STATISTICS_TOP_LEFT[0], STATISTICS_TOP_LEFT[1]), stats,
                            fill=TEXT_COLOR, font=ImageFont.truetype("arial.ttf", 50))

        self.images.append(img)
        img.save("{}{}{}".format(self.save_images_dir, str(step).zfill(4), '.png'))
