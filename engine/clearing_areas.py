from engine.game_parameters import BOARD_SIZE, SQUARE_SIZE

# areas to clear when full: rows, cols, 3x3 squares
clearing_areas = []
clearing_areas.extend([[(row, col) for row in range(BOARD_SIZE)] for col in range(BOARD_SIZE)])
clearing_areas.extend([[(col, row) for row in range(BOARD_SIZE)] for col in range(BOARD_SIZE)])
clearing_areas.extend(
    [[(row, col) for row in range(s_row, s_row + SQUARE_SIZE) for col in range(s_col, s_col + SQUARE_SIZE)]
     for s_row in range(0, BOARD_SIZE, SQUARE_SIZE) for s_col in range(0, BOARD_SIZE, SQUARE_SIZE)])
