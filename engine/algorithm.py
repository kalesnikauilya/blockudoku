import math
from copy import deepcopy


# DFS_NODES_LIMIT = math.inf
DFS_NODES_LIMIT = 100


# pool = Pool(4)


class Algorithm:
    def __init__(self, board):
        self.board = deepcopy(board)
        self.best_evaluation = -1
        self.best_move_seq = []
        self.cnt = 0

        #self.best_ev = multiprocessing.Value('d', -1.0)
        #self.best_mo = multiprocessing.Array()

    def dfs(self, cur_board, move_seq):
        if self.cnt % 10000 == 0:
            print("{} {}".format(self.cnt, len(cur_board.available_pieces)))
        self.cnt = self.cnt + 1
        valid_moves = cur_board.get_all_valid_moves()
        if len(valid_moves) == 0:
            evaluation = self.evaluate_board(cur_board)
            if evaluation > self.best_evaluation:
                self.best_evaluation = evaluation
                self.best_move_seq = move_seq
            return

        for valid_move in valid_moves:
            c_board = deepcopy(cur_board)
            c_move_seq = deepcopy(move_seq)
            c_board.place_piece(valid_move[0], valid_move[1])
            c_move_seq.append(valid_move)
            if self.cnt > DFS_NODES_LIMIT:
                return
            self.dfs(c_board, c_move_seq)

    def evaluate_board(self, board):
        val = sum(x.count(1) for x in board.grid)

        return (81 - val) / 81 * (1 + board.score - self.board.score) / board.count_connected_components()

    def get_move(self):
        self.dfs(self.board, [])

        # freeze_support()
        # pool.map_async(self.dfs_p, (self.board, []))

        return self.best_move_seq
