from engine.game_parameters import PIECE_SIZE

pieces_list = [
    [(0, 0)],  # 1x dot

    [(0, 0), (0, 1)],  # 2x line horizontal
    [(0, 0), (1, 0)],  # 2x line vertical
    [(0, 0), (1, 1)],  # 2x line diagonal top-left to bottom-right
    [(0, 1), (1, 0)],  # 2x line diagonal bottom-left to top-right

    [(0, 0), (0, 1), (0, 2)],  # 3x line horizontal
    [(0, 0), (1, 0), (2, 0)],  # 3x line vertical
    [(0, 0), (1, 1), (2, 2)],  # 3x line diagonal top-left to bottom-right
    [(0, 2), (1, 1), (2, 0)],  # 3x line diagonal bottom-left to top-right
    [(0, 0), (0, 1), (1, 1)],  # 3x small corner top-right
    [(0, 1), (1, 1), (1, 0)],  # 3x small corner bottom-right
    [(1, 1), (1, 0), (0, 0)],  # 3x small corner bottom-left
    [(1, 0), (0, 0), (0, 1)],  # 3x small corner top-left

    [(0, 0), (0, 1), (1, 0), (1, 1)],  # 4x square
    [(0, 0), (0, 1), (0, 2), (0, 3)],  # 4x line horizontal
    [(0, 0), (1, 0), (2, 0), (3, 0)],  # 4x line vertical
    [(0, 0), (1, 1), (2, 2), (3, 3)],  # 4x line diagonal top-left to bottom-right
    [(0, 3), (1, 2), (2, 1), (3, 0)],  # 4x line diagonal bottom-left to top-right
    [(0, 0), (1, 0), (2, 0), (2, 1)],  # 4x L-shape bottom-left
    [(1, 0), (0, 0), (0, 1), (0, 2)],  # 4x L-shape top-left
    [(0, 0), (0, 1), (1, 1), (2, 1)],  # 4x L-shape top-right
    [(0, 2), (1, 2), (1, 1), (1, 0)],  # 4x L-shape bottom-right
    [(0, 1), (1, 1), (2, 1), (2, 0)],  # 4x mirrored L-shape bottom-right
    [(0, 0), (1, 0), (1, 1), (1, 2)],  # 4x mirrored L-shape bottom-left
    [(2, 0), (1, 0), (0, 0), (0, 1)],  # 4x mirrored L-shape top-left
    [(0, 0), (0, 1), (0, 2), (1, 2)],  # 4x mirrored L-shape top-right
    [(0, 0), (1, 0), (1, 1), (2, 1)],  # 4x Z-shape down-right-down
    [(0, 1), (1, 1), (1, 0), (2, 0)],  # 4x Z-shape down-left-down
    [(0, 0), (0, 1), (1, 1), (1, 2)],  # 4x Z-shape right-down-right
    [(1, 0), (1, 1), (0, 1), (0, 2)],  # 4x Z-shape right-up-right

    [(0, 1), (1, 0), (1, 1), (1, 2), (2, 1)],  # 5x cross
    [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4)],  # 5x line horizontal
    [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0)],  # 5x line vertical
    [(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)],  # 5x large corner bottom-left
    [(2, 0), (1, 0), (0, 0), (0, 1), (0, 2)],  # 5x large corner top-left
    [(0, 0), (0, 1), (0, 2), (1, 2), (2, 2)],  # 5x large corner top-right
    [(0, 2), (1, 2), (2, 2), (2, 1), (2, 0)],  # 5x large corner bottom-right
    [(0, 1), (0, 0), (1, 0), (2, 0), (2, 1)],  # 5x C-shape right
    [(1, 0), (0, 0), (0, 1), (0, 2), (1, 2)],  # 5x C-shape bottom
    [(0, 0), (0, 1), (1, 1), (2, 1), (2, 0)],  # 5x C-shape left
    [(0, 0), (1, 0), (1, 1), (1, 2), (0, 2)],  # 5x C-shape top

    [(0, 0), (0, 1), (0, 2), (1, 1), (2, 1)],  # 5x T-shape bottom
    [(0, 0), (1, 0), (2, 0), (1, 1), (1, 2)],  # 5x T-shape right
    [(2, 0), (2, 1), (2, 2), (1, 1), (0, 1)],  # 5x T-shape top
    [(0, 2), (1, 2), (2, 2), (1, 1), (1, 0)]   # 5x T-shape left

]


def get_piece_rect(piece):
    piece_rect = [[0 for _ in range(PIECE_SIZE)] for _ in range(PIECE_SIZE)]
    for row, col in piece:
        piece_rect[row][col] = 1

    return piece_rect


pieces_rect = [get_piece_rect(piece) for piece in pieces_list]


def print_pieces_list():
    for piece_id in range(len(pieces_list)):
        print('\n'.join([''.join(['{:1}'.format(item) for item in row]) for row in pieces_rect[piece_id]]))
        print()
