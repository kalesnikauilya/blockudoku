# board
BOARD_SIZE = 9
MAX_PIECES = 3
SQUARE_SIZE = 3
PIECE_SIZE = 5

# scoring
STREAK_BONUS_MX = 10
SIMULTANEOUS_BONUS_MX = 10
CLEARED_AREA_BONUS_MX = 18


def calculate_score(piece, cleared_areas_cnt, streak):
    return calculate_piece_score(piece) + calculate_streak_score(streak) + \
           calculate_cleared_areas_score(cleared_areas_cnt)


def calculate_piece_score(piece):
    return len(piece)


def calculate_streak_score(streak):
    return STREAK_BONUS_MX * streak


def calculate_cleared_areas_score(cleared_areas_cnt):
    return CLEARED_AREA_BONUS_MX * cleared_areas_cnt + SIMULTANEOUS_BONUS_MX * (max(0, cleared_areas_cnt - 1))
