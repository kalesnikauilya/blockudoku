import random
from copy import deepcopy

from engine.algorithm import Algorithm
from engine.clearing_areas import clearing_areas
from engine.game_parameters import BOARD_SIZE, MAX_PIECES, calculate_score
from engine.pieces import pieces_list, pieces_rect
from engine.visualizer import Visualizer

d_row = [0, 0, 1, -1]
d_col = [1, -1, 0, 0]

USE_ALGORITHM = True
CREATE_IMAGES = True
SEED = 424243


class Game:
    def __init__(self):
        self.board = Board(BOARD_SIZE)
        self.step = 0
        self.visualizer = Visualizer(SEED)
        random.seed(SEED)

    def start(self):
        algorithm_seq = []
        while True:
            if len(self.board.available_pieces) == 0:
                self.board.generate_available_pieces()
            self.print_step()
            if CREATE_IMAGES:
                self.visualizer.create_image(self.step, self.board)

            if not self.board.have_valid_placement():
                print('No valid moves, game over.')
                print("{}{}".format("Final score = ", self.board.score))
                if CREATE_IMAGES:
                    self.visualizer.create_video()
                break

            if USE_ALGORITHM:
                if len(algorithm_seq) == 0:
                    algorithm = Algorithm(self.board)
                    algorithm_seq = algorithm.get_move()

                piece_number, (row, col) = algorithm_seq[0]
                self.board.place_piece(piece_number, (row, col))
                algorithm_seq.remove((piece_number, (row, col)))
            else:
                piece_number, row, col = map(int, input().split())
                self.board.place_piece(piece_number, (row, col))

            self.step = self.step + 1

    def print_step(self):
        print("{}{}".format("Step = ", self.step))
        print("{}{}".format("Score = ", self.board.score))
        print(self.board.get_all_valid_moves(self.board.available_pieces))
        print("{}{}".format("Valid moves cnt = ", len(self.board.get_all_valid_moves(self.board.available_pieces))))
        print("{}{}".format("Connected components = ", self.board.count_connected_components()))
        self.board.print()
        self.board.print_available_pieces()


class Board:
    def __init__(self, size):
        self.size = size
        self.score = 0
        self.streak = 0
        self.grid = [[0 for _ in range(size)] for _ in range(size)]
        self.available_pieces = []
        self.ordered_pieces = []

    def _set_position(self, position, value):
        self.grid[position[0]][position[1]] = value

    def generate_available_pieces(self):
        self.available_pieces = [random.randrange(0, len(pieces_list) - 1) for _ in range(MAX_PIECES)]
        self.ordered_pieces = deepcopy(self.available_pieces)

    def print(self):
        print('\n'.join([''.join(['{:2}'.format(item) for item in row]) for row in self.grid]))
        print()

    def print_available_pieces(self):
        for piece in self.available_pieces:
            print('\n'.join([''.join(['{:1}'.format(item) for item in row]) for row in pieces_rect[piece]]))
            print()

    def place_piece(self, piece_number, position):
        piece = pieces_list[self.available_pieces[piece_number]]
        assert self.is_valid_placement(piece, position)
        for element in piece:
            element_position = [sum(x) for x in zip(position, element)]
            self._set_position(element_position, 1)
        cleared_areas_cnt = self.clear_full_areas()
        self.update_score(piece, cleared_areas_cnt)
        self.update_streak(cleared_areas_cnt)
        self.available_pieces.remove(self.available_pieces[piece_number])

    def update_score(self, piece, cleared_areas_cnt):
        self.score = self.score + calculate_score(piece, cleared_areas_cnt, self.streak)

    def update_streak(self, cleared_areas_cnt):
        if cleared_areas_cnt == 0:
            self.streak = 0
        else:
            self.streak = self.streak + 1

    def get_areas_to_clear(self):
        areas_to_clear = []
        for area in clearing_areas:
            has_zero = False
            for area_id, (row, col) in enumerate(area):
                if self.grid[row][col] == 0:
                    has_zero = True
                    break

            if not has_zero:
                areas_to_clear.append(area)

        return areas_to_clear

    def clear_full_areas(self):
        areas_to_clear = self.get_areas_to_clear()
        for area in areas_to_clear:
            for pos in area:
                self._set_position(pos, 0)

        return len(areas_to_clear)

    def is_free(self, position):
        return self.grid[position[0]][position[1]] == 0

    def is_inside_board(self, position):
        return 0 <= position[0] < self.size and 0 <= position[1] < self.size

    def is_valid_placement(self, piece, position):
        for element in piece:
            element_position = [sum(x) for x in zip(position, element)]
            if not self.is_inside_board(element_position):
                return False
            if not self.is_free(element_position):
                return False

        return True

    def have_valid_placement(self, piece_ids=None):
        if piece_ids is None:
            piece_ids = self.available_pieces

        for piece_number, piece_id in enumerate(piece_ids):
            piece = pieces_list[piece_id]
            for row in range(self.size):
                for col in range(self.size):
                    if self.is_valid_placement(piece, (row, col)):
                        return True

        return False

    def get_all_valid_moves(self, piece_ids=None):
        if piece_ids is None:
            piece_ids = self.available_pieces

        valid_moves = []
        for piece_number, piece_id in enumerate(piece_ids):
            piece = pieces_list[piece_id]
            for row in range(self.size):
                for col in range(self.size):
                    if self.is_valid_placement(piece, (row, col)):
                        valid_moves.append((piece_number, (row, col)))

        return valid_moves

    def _dfs(self, row, col, components, cur_color):
        components[row][col] = cur_color
        for direction in range(4):
            n_row = row + d_row[direction]
            n_col = col + d_col[direction]
            if self.is_inside_board((n_row, n_col)):
                if components[n_row][n_col] == -1 and self.is_free((n_row, n_col)):
                    self._dfs(n_row, n_col, components, cur_color)

    def count_connected_components(self):
        components = [[-1 for _ in range(self.size)] for _ in range(self.size)]
        cur_color = 0
        for row in range(self.size):
            for col in range(self.size):
                if components[row][col] == -1 and self.is_free((row, col)):
                    self._dfs(row, col, components, cur_color)
                    cur_color = cur_color + 1

        return cur_color
