import cProfile
import sys

from engine.game import Game


def run(profiler=False):
    pr = cProfile.Profile()
    pr.enable()

    game = Game()
    game.start()

    pr.disable()
    if profiler:
        pr.print_stats(sort="calls")


if __name__ == '__main__':
    sys.setrecursionlimit(10000)
    run(True)
